import React from 'react';
import './App.css';
import Header from './components/header/header'
import Header_home from './components/header/Header_home'
import Login from './components/login/Login'
import Logout from './components/logout/Logout'
import Register from './components/register/Register'
import PrivateRoute from './helper/PrivateRoute'

import Product_Table from './components/product/Product'
import Product_View from './components/product/ProductView'

import Home from './components/home/Home'

import 'bootstrap/dist/css/bootstrap.min.css';
import { Route, Switch, Redirect } from 'react-router-dom'

var routes = {
  login: '/login',
  register: '/register',
  home:'/home',
  logout:'/logout',
  product:'/product',
  product_view:'/product_view/:id'
}


function App() {
  let user = localStorage.getItem('user')
  return (
    <div>
      {!user ? <Header/> : <Header_home/>}
      
        
        <div className="container">
          <Switch>
            <Redirect exact from="/" to={routes.login}></Redirect>
            <Route exact path={routes.login} component={Login}></Route>
            <Route exact path={routes.register} component={Register}></Route>
          </Switch>
        </div>
        <div className="container">
          <Switch>
              <PrivateRoute exact path={routes.home} component={Home}></PrivateRoute>
              <PrivateRoute exact path={routes.product} component={Product_Table}></PrivateRoute>
              <PrivateRoute exact path={routes.product_view} component={Product_View}></PrivateRoute>
              <PrivateRoute exact path={routes.logout} component={Logout}></PrivateRoute>
              {/* <PrivateRoute exact path={routes.create} component={Create}></PrivateRoute>
              <PrivateRoute exact path={routes.edit} component={Edit}></PrivateRoute> */}
          </Switch>
        </div>
        
    </div>
  );
}

export default App;
