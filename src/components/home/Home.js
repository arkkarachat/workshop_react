import React,{useEffect} from 'react'

export default function Home(props) {
    let user =JSON.parse(localStorage.getItem('user'))   
    // console.log(user[0].id);

    // localStorage.clear();
    useEffect(() => {
        if(!localStorage.getItem('user')){
            props.history.push('/login')
        }
    })
    return (
        <div>
            Home
        </div>
    )
}
