import React, {useState,useEffect}from 'react'
import  "./Login.css"

import {UserLogin } from '../../api/api';

export default function Login(props) {
    const [username, setUsername] = useState('')
    const [password, setPassword] = useState('')

   

    const save = async (e) =>{
        e.preventDefault();
        let user = {
            username: username,
            password: password
        }

        console.log(user);
        

        let result = await UserLogin(user)
        // console.log(result);
        var data_user ={
            id : result.data._id
        }
        
        // console.log(JSON.stringify(data_user));
        
        if(result.status==='success'){
            localStorage.setItem('user',JSON.stringify(data_user))
            props.history.push('/home')
        }else if(result.status==='error'){
            alert(result.message)
        }
    }
    useEffect(() => {
        
        if(localStorage.getItem('user')){
            props.history.push('/home')
        }
       
    })
     
    return (
        <div className="container login">
            <div className="row">
                <div className="col-12 img">
                    <img className="img-calculate" width="100" src={process.env.PUBLIC_URL+"images/login/unlock.png"}></img>
                </div>
            </div>
            <form onSubmit={save}>
            <div className="row">
                <div className="col-12">
                    <div className="form-group">
                        <label>Username</label>
                        <input type="text" className="form-control" onChange={(e) => setUsername(e.target.value)} />
                    </div>
                    <div className="form-group">
                        <label >Password</label>
                        <input type="password" className="form-control" onChange={(e) => setPassword(e.target.value)}/>
                    </div>
                </div>
            </div>
            <div className="row">
                <div className="col-12">
                    <button type="submit" className="btn btn-primary btn-block">Login</button>
                </div>
            </div>
            </form>
        </div>
    )
}
