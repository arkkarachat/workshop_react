import React ,{ useState, useEffect }from 'react'
import './Register.css'
import {registerUser } from '../../api/api';
export default function Register(props) {
    const [username, setUsername] = useState('')
    const [password, setPassword] = useState('')
    const [name, setName] = useState('')
    const [age, setAge] = useState(0)
    const [salary, setSalary] = useState(0)

    const insertUser  = async (e) => {
        e.preventDefault();
        let user = {
            username: username,
            password: password,
            name: name,
            age: age,
            salary: salary
        }
        let result = await registerUser(user)
        // console.log(result)
        // console.log(user)
        if(result.status==='sucecss'){
            alert('สมัครเรียบร้อยแล้ว')
            // setUsername(null)
            // setPassword = ''
            // setName = ''
            // setAge = 0
            // setSalary = 0
            props.history.push('/login')
        }
    }

    // useEffect(() => {
    //     console.log('object')
    //     // return () => {
           
    //     // }
    // })
    return (
        <div className="container register">
            <div className="row">
                <div className="col-12 img">
                    <img className="img-calculate" width="100" src={process.env.PUBLIC_URL+"images/register/register.png"}></img>
                </div>
            </div>
            <form onSubmit={insertUser}>
            <div className="row">
                <div className="col-12">
                    <div className="form-group">
                        <label >Username</label>
                        <input type="text" className="form-control"  onChange={(e) => setUsername(e.target.value)}/>
                    </div>
                    <div className="form-group">
                        <label >Password</label>
                        <input type="password" className="form-control"  onChange={(e) => setPassword(e.target.value)}/>
                    </div>
                    <div className="form-group">
                        <label >Name</label>
                        <input type="text" className="form-control"  onChange={(e) => setName(e.target.value)}/>
                    </div>
                    <div className="form-group">
                        <label >Age</label>
                        <input type="text" className="form-control"  onChange={(e) => setAge(e.target.value)}/>
                    </div>
                    <div className="form-group">
                        <label >Salary</label>
                        <input type="text" className="form-control"  onChange={(e) => setSalary(e.target.value)}/>
                    </div>
                </div>
            </div>
            <div className="row">
                <div className="col-12">
                    <button type="submit" className="btn btn-success btn-block">Register</button>
                </div>
            </div>
            </form>
        </div>
    )
}
