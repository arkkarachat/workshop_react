Git global setup
git config --global user.name "Arkkarachat  Siribout"
git config --global user.email "arkkarachat.s@gmail.com"


Create a new repository
git clone git@gitlab.com:arkkarachat/workshop_react.git
cd workshop_react
touch README.md
git add README.md
git commit -m "add README"
git push -u origin master

Push an existing folder
cd existing_folder
git init
git remote add origin git@gitlab.com:arkkarachat/workshop_react.git
git add .
git commit -m "Initial commit"
git push -u origin master


cd existing_repo
git remote rename origin old-origin
git remote add origin git@gitlab.com:arkkarachat/workshop_react.git
git push -u origin --all
git push -u origin --tags